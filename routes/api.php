<?php

use Illuminate\Http\Request;
use App\Student;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('auth/login', 'AuthController@login');
Route::post('auth/register', 'AuthController@register');
Route::group(['middleware' => 'jwt.auth'], function(){

    Route::put('/studentClasses/{id}', 'StudentClassesController@update');

    Route::get('/users', 'UsersController@index');
    Route::put('/users/changePassword', 'UsersController@updatePassword');
    Route::get('/venues', 'VenueController@index');

    Route::get('/schoolStats', 'SchoolsController@getStats');
    Route::get('/{user_type}/{user_id}/classes', 'ClassesController@getClasses');
    Route::resource('classes', 'ClassesController');
    Route::resource('lecturers', 'LecturersController');
    Route::get('{class_id}/students', 'ClassesController@getStudents')->name('getStudentsInClass');
    Route::get('/search/{serch_term}', 'StudentsController@autocompleteSearch');
    Route::get('/{school_id}/lecturers', 'SchoolsController@getLecturers');
    Route::resource('students', 'StudentsController');

    Route::resource('admins', 'AdminsController');
    Route::get('/schools', 'SchoolsController@index');
    Route::get('/{class_id}/attendance', 'ClassesController@getAttendance');

    Route::resource('studentAttendance', 'StudentAttendanceController');
    Route::resource('studentMarks', 'StudentMarksController');

    Route::post('auth/mail', 'AuthController@sendMail');
  Route::get('auth/user', 'AuthController@user');
  Route::post('fees', 'FeesController@store');
  Route::get('fees', 'FeesController@store');
});

Route::group(['middleware' => 'jwt.refresh'], function(){
  Route::get('auth/refresh', 'AuthController@refresh');
});

