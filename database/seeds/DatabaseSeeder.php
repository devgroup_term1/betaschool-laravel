<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $schools = [
            ['name' => "Creative Technologies"],
            ['name' => "Visual Communication"],
            ['name' => "Film Arts"]
        ];
        App\School::insert($schools);

        $students = array();
        $lecturers = array();
        $admins = array();

        $numStudents = 100;
        $numAdmins = 20;
        $numLecturers = $numAdmins;

        $numVenues;
        $numClasses;
        $numVenues = 25;


        $venues = factory(App\Venue::class, $numVenues)->create();

        //ARTHUR BAHR (lecturer)
        factory(App\User::class)->create(['email' => 'bahr.a@university.com', 'password' => bcrypt("123456"), 'remember_token' => str_random(10), 'role' => 'lecturer'])->each(function ($user){
            factory(App\Lecturer::class)->create(['user_id' => $user->id,'title' => 'Dr.','firstName' => 'Arthur', 'lastName' => 'Bahr', 'gender' => 'male', 'qualifications' => 'Doctor of Philospphy (Ph.D.), English Language and Literature/Letters', 'school_id' => 2, 'dob' => '1981-10-12'])->each(function($lecturer){
                factory(App\Classes::class)->create(['id'=>1,'venue_id'=>7,'lecturer_id'=>$lecturer->user_id,'name'=>'Creative Music Technologies','startTime'=>9,'duration'=>2,'day'=>'Monday','price'=>59254,'school_id'=>2,]);
                factory(App\Classes::class)->create(['id'=>2,'venue_id'=>7,'lecturer_id'=>$lecturer->user_id,'name'=>'Philosophy Of Action','startTime'=>'13','duration'=>'4','day'=>'Monday','price'=>59254,'school_id'=>2,]);
                factory(App\Classes::class)->create([
                    'id'=>3,
                    'venue_id'=>4,
                    'lecturer_id'=>$lecturer->user_id,
                    'name'=>'Ethnozoology',
                    'startTime'=>'9',
                    'duration'=>'4',
                    'day'=>'Tuesday',
                    'price'=>49500,
                    'school_id'=>2,
                ]);
                factory(App\Classes::class)->create([
                    'id'=>4,
                    'venue_id'=>1,
                    'lecturer_id'=>$lecturer->user_id,
                    'name'=>'Financial Econometrics',
                    'startTime'=>'11',
                    'duration'=>'2',
                    'day'=>'Thursday',
                    'price'=>34500,
                    'school_id'=>2,
                ]);
                factory(App\Classes::class)->create([
                    'id'=>5,
                    'venue_id'=>9,
                    'lecturer_id'=>$lecturer->user_id,
                    'name'=>'Media Studies And Communication',
                    'startTime'=>'14',
                    'duration'=>'3',
                    'day'=>'Thursday',
                    'price'=>27800,
                    'school_id'=>2,
                ]);
                factory(App\Classes::class)->create([
                    'id'=>6,
                    'venue_id'=>3,
                    'lecturer_id'=>$lecturer->user_id,
                    'name'=>'Computer Security And Reliability',
                    'startTime'=>'13',
                    'duration'=>'2',
                    'day'=>'Friday',
                    'price'=>52300,
                    'school_id'=>2,
                ]);
            });
        });

        
        //Monica Smith (student)
       
                
        factory(App\User::class, 9)->create(['role'=>'lecturer'])->each(function ($user){
            factory(App\Lecturer::class)->create(['user_id' => $user->id]);
        });

        factory(App\Admin::class)->create([
                    'id'=>6,
                    'venue_id'=>3,
                    'lecturer_id'=>$lecturer->user_id,
                    'name'=>'Computer Security And Reliability',
                    'startTime'=>'13',
                    'duration'=>'2',
                    'day'=>'Friday',
                    'price'=>52300,
                    'school_id'=>2,
                ]);
        factory(App\User::class, 10)->create(['role'=>'admin'])->each(function ($user){
            factory(App\Admin::class)->create(['user_id' => $user->id]);
        });;
         $monica = factory(App\User::class)->create(['email' => 'm.smith@university.com', 'password' => bcrypt("123456"), 'remember_token' => str_random(10), 'role' => 'student']);
        factory(App\Student::class)->create(['user_id' => $monica->id,'firstName' => 'Monica','lastName' => 'Smith','gender' => 'female','dob' => '1998-06-16','school_id' => 2,'contactNum' => '0764910361','year' => 2]);
        factory(App\User::class, 49)->create(['role'=>'student'])->each(function ($user){
            factory(App\Student::class)->create(['user_id' => $user->id]);
        });;

        for($r = 1; $r < 7; $r++){
            for ($x = 20; $x <=70; $x++){
                DB::table('student_classes')->insert([
                    'student_id' => $x,
                    'class_id' => $r,
                ]);
            }
        }

        

        //factory(App\Classes::class, 50)->create();

        /*

        $timeSlots = [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18];
        $durations = [1,2,3,4];

        $count = 0;
        $classes = [];
        $first = [];
        $faker = \Faker\Factory::create();
        $d = ["Monday","Tuesday","Wednesday","Thursday","Friday",];
        

        while($count < 50) {
            $lecturerID = $faker->numberBetween(2, 20);
            $startTime = $faker->randomElement($timeSlots);
            $duration = $faker->randomElement($durations);
            $day = $faker->randomElement($d);
            if ($startTime + $duration > 18){ $startTime = 18 - $duration;}
            $item = [
                'venue_id' => $faker->numberBetween($min = 1, $numVenues),
                'lecturer_id' => $lecturerID,
                'startTime' => $startTime,
                'duration' => $duration,
                'day' => $day,
                'price' => $faker->randomNumber($nbDigits = 6, $strict = false),
                'name' => $faker->word,
                'school_id' => DB::table('lecturers')->where('user_id', $lecturerID)->value('school_id')
            ];

            while( (array_search($item['day'], array_column($classes, 'day'))) &&
                (array_search($item['startTime'], array_column($classes, 'startTime'))) &&
                (array_search($item['lecturer_id'], array_column($classes, 'lecturer_id')))
            ){
                $item['lecturer_id'] = $faker->numberBetween(2, 20);
            }

            //print_r(array_search($item['startTime'], array_column($classes, 'startTime'))."\n");

            array_push($classes, $item);
            $count++;
        }

        
        DB::table('classes')->insert($classes);

        for ($x = 0; $x <=100; $x++){
            for ($r = 0; $r <=25; $r++){
                $p = mt_rand(41, 140);
                DB::table('student_classes')->insert([
                    'student_id' => $p,
                    'class_id' => $x+1,
                ]);
            }
        }

        
        */
    }
}