<?php

use Faker\Generator as Faker;

$factory->define(App\StudentMark::class, function (Faker $faker) {
    return [
        'student_id' => $faker->numberBetween($min = 1, $max = 10),
        'class_id' => $faker->numberBetween($min = 1, $max = 10),
        'term' => $faker->numberBetween($min = 1, $max = 4),
        'mark' => $faker->numberBetween($min = 0, $max = 100),
    ];
});