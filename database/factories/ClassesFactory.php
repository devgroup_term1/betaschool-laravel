<?php

use Faker\Generator as Faker;

$factory->define(App\Classes::class, function (Faker $faker) {
    $school = $faker->randomElement([1, 2, 3]);
    $timeSlots = [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18];
    $durations = [1,2,3,4];
    return [
        'venue_id' => $faker->numberBetween($min = 1, $max = 10),
        'lecturer_id' => $faker->numberBetween($min = 1, $max = 20),
        'startTime' => $faker->numberBetween($min = 1, $max = 10),
        'duration' => $faker->numberBetween(1, 4),
        'day' => $faker->dayOfWeek($max = 'now'),
        'price' => $faker->randomNumber($nbDigits = 6, $strict = false),
        'name' => $faker->word,
        'school_id' => $school
    ];
});
