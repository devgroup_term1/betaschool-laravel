<?php

use Faker\Generator as Faker;

$factory->define(App\Admin::class, function (Faker $faker) {
    $gender = $faker->randomElement(['male', 'female']);
    return [
        'firstName' => $faker->firstName($gender),
        'lastName' => $faker->lastName,
    ];
});
