<?php

use Faker\Generator as Faker;

$factory->define(App\Lecturer::class, function (Faker $faker) {
    $gender = $faker->randomElement(['male', 'female']);
    $school = $faker->randomElement([1, 2, 3]);
    return [
        'title' => $faker->title($gender),
        'firstName' => $faker->firstName($gender),
        'lastName' => $faker->lastName,
        'gender' => $gender,
        'dob' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'qualifications' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'school_id' => $school
    ];
});
