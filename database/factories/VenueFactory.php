<?php

use Faker\Generator as Faker;

$factory->define(App\Venue::class, function (Faker $faker) {
    return [
        'venueName' => $faker->word,
        'capacity' => $faker->numberBetween($min = 10, $max = 200)
    ];
});
