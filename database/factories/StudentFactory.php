<?php

use Faker\Generator as Faker;

$factory->define(App\Student::class, function (Faker $faker) {
    $gender = $faker->randomElement(['male', 'female']);
    $school = $faker->randomElement([1, 2, 3]);
    return [
        'studentNum' => $faker->unique()->randomNumber($nbDigits = 6),
        'firstName' => $faker->firstName($gender),
        'lastName' => $faker->lastName,
        'gender' => $gender,
        'dob' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'contactNum' => $faker->e164PhoneNumber,
        'school_id' => $school,
        'year' => $school
    ];
});            