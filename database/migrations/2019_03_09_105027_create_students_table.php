<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->integer('user_id')->primary();
            $table->string('studentNum')->unique();
            $table->string('firstName');
            $table->string('lastName');
            $table->char('gender');
            $table->date('dob');
            $table->string('contactNum');
            $table->integer('school_id')->unsigned();
            $table->integer('year');
        });
        Schema::table('lecturers', function($table) {
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
