<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLecturersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lecturers', function (Blueprint $table) {
            $table->integer('user_id')->primary();
            $table->string('title');
            $table->string('firstName');
            $table->string('lastName');
            $table->char('gender');
            $table->text('qualifications');
            $table->integer('school_id')->unsigned();
            $table->date('dob');
        });
        Schema::table('lecturers', function($table) {
            $table->foreign('school_id')->references('id')->on('schools');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lecturers');
    }
}
