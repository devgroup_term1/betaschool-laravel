<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('venue_id')->unsigned();
            $table->integer('lecturer_id');
            $table->string('name');
            $table->integer('startTime');
            $table->integer('duration');
            $table->string('day');
            $table->decimal('price', 8, 2);
            $table->integer('school_id')->unsigned();
            
        });
        Schema::table('classes', function($table) {
            $table->foreign('lecturer_id')->references('user_id')->on('lecturers');
            $table->foreign('venue_id')->references('id')->on('venues');
            $table->foreign('school_id')->references('id')->on('schools');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classes');
    }
}
