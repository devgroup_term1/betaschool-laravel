<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentAttendance extends Model
{
    protected $attributes = [
        'attendance' => 100
    ];

    protected $table = 'student_attendance';

    public function student(){
        return $this->belongsTo('App\Student');
    }

    public function class(){
        return $this->belongsTo('App\Classes');
    }
}
