<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lecturer extends Model
{
    protected $primaryKey = 'user_id';
    public $timestamps = false;

    public function classes(){
        return $this->hasMany('App\Classes', 'lecturer_id', 'user_id');
    }

    public function school(){
        return $this->belongsTo('App\School');
    }

    /*
    public function user()
    {
        return $this->morphOne('App\User', 'userDetails');
    }
    */
}
