<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $primaryKey = 'user_id';
    public $timestamps = false;

    public function user()
    {
        return $this->morphOne('App\User', 'userDetails');
    }
}
