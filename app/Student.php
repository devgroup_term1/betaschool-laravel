<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $primaryKey = 'user_id';
    public $timestamps = false;

    public function marks(){
        return $this->hasMany('App\StudentMark');
    }

    public function attendance(){
        return $this->hasMany('App\StudentAttendance');
    }

    public function classes(){
        return $this->belongsToMany('App\Classes', 'student_classes', 'student_id', 'class_id');
    }

    public function user()
    {
        return $this->morphOne('App\User', 'userDetails');
    }

    public function school(){
        return $this->hasOne('App\School');
    }
}
