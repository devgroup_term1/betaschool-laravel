<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venue extends Model
{
    //
    public $timestamps = false;

    public function classes(){
        return hasMany('App\Classes');
    }
}
