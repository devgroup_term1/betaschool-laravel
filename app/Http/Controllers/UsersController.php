<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UsersController extends Controller
{
    public function index()
    {
        return User::all();
    }

    public function destroy($id)
    {
        User::destroy($id);
    }

    public function updatePassword(Request $request){
        $user = User::find($request->user_id);
        $user->password = bcrypt($request->password);
        $user->save();
    }
}
