<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\School;

class SchoolsController extends Controller
{
    public function getLecturers($id){
        return School::find($id)->lectures;
    }

    public function index(){
        return School::all();
    }

    public function getStats(){
        return [
            "Creative Technologies" => count(School::find(1)->students),
            "Visual Communication" => count(School::find(2)->students),
            "Film Arts" => count(School::find(3)->students)
        ];
    }
}
