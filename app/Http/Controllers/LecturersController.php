<?php

namespace App\Http\Controllers;
use App\Lecturer;

use Illuminate\Http\Request;
use App\Http\Resources\LecturerCollection;

class LecturersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //This is eloquent
        return new LecturerCollection(Lecturer::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $lecturer = new Lecturer;
        $lecturer->firstName = $request->firstName;
        $lecturer->lastName = $request->lastName;
        $lecturer->user_id = $request->user_id;
        $lecturer->dob= $request->dob;
        $lecturer->gender= $request->gender;
        $lecturer->title= $request->title;
        $lecturer->qualifications= $request->qualifications;
        $lecturer->school_id= $request->school_id;
        $lecturer->save();
        return response([
            'status' => 'success',
            'data' => $lecturer
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        if( Lecturer::find($id)!= null){

            return Lecturer::find($id);
        }else{
            return ['id' => 0];
        }
    }

    public function getClasses($id){
        return Lecturer::find($id)->classes;
        //return Lecturer::withCount('classes')->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Lecturer::destroy($id);
    }
}
