<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\StudentClasses;

class StudentClassesController extends Controller
{
    public function update(Request $request, $id){
        $students = $request->student;
        $studentData = [];

        foreach($students as $student){
            array_push($studentData, array('class_id' => $id, 'student_id' => $student['user_id']));
        }

        StudentClasses::where('class_id', $id)->delete();
        StudentClasses::insert($studentData);
    }
}
