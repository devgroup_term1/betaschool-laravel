<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StudentMark;

class StudentMarksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return StudentMarks::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*
        $validatedData = $request->validate([
                'student_id' => 'required|integer',
                'class_id' => 'required|integer',
                'term' => 'required|integer',
                'mark' => 'required|integer'
            ]);
            */

        $record = new StudentMark;

        $studentMarks = $request->students;

        foreach($studentMarks as $studentMark){
            $record->student_id = $studentMark['user_id'];
            $record->class_id = $request->id;
            $record->term = $request->term;
            $record->mark = $studentMark['mark'];
            $record->save();
        }


        if($record->save()){
            return Response('mark recorded');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(StudentMark::find($id)){
            return StudentMark::find($id);
        }else{
            return Response('Sutdent not found');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updatedMark = StudentMark::find($id);

        $updatedMark->student_id = $request['student_id'];
        $updatedMark->class_id = $request['class_id'];
        $updatedMark->term = $request['term'];
        $updatedMark->mark = $request['mark'];

        if($updatedMark->save()){
            return response('Mark Updated', 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
