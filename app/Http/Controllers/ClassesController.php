<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classes;
use App\Student;
use App\Lecturer;
use Validator;
use App\Http\Resources\Attendance as AttendanceResource;
use App\Http\Resources\AttendanceCollection;
use App\Http\Resources\ClassesCollection;

class ClassesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new ClassesCollection(Classes::all());
        //return Classes::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Booking validation (to ensure that there are no double bookings)

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request;

        $newClass = new Classes;
        $newClass->venue_id = $data['venue_id'];
        $newClass->lecturer_id = $data['lecturer_id'];
        $newClass->name = $data['name'];
        $newClass->startTime = $data['startTime'];
        $newClass->duration = $data['endTime'];
        $newClass->day = $data['day'];
        $newClass->price = $data['price'];
        $newClass->school_id = $data['school_id'];

        if($newClass->save()){
            return $newClass;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Classes::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updatedClass = Classes::find($id);

        $updatedClass->venue_id = $request['venue']['id'];
        $updatedClass->lecturer_id = $request['lecturer']['user_id'];
        $updatedClass->name = $request['name'];
        $updatedClass->startTime = $request['startTime'];
        $updatedClass->duration = $request['duration'];
        $updatedClass->day = $request['day'];
        $updatedClass->price = $request['price'];
        $updatedClass->school_id = $request['school']['id'];

        if($updatedClass->save()){
            return response('Class Updated', 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Classes::destroy($id)){
            return response('Class Deleted', 200);
        }
    }

    public function getStudents($id){
        return Classes::find($id)->students;
    }

    public function getAttendance($id){
        $attendance = Classes::find($id)->attendance;
        return new AttendanceCollection($attendance);
    }

    public function getClasses($userType, $userId){
        if($userType == "lecturer"){
            return new ClassesCollection(Lecturer::find($userId)->classes);
        }else if ($userType == "student"){
            return new ClassesCollection(Student::find($userId)->classes);
        }else{
            return null;
        }
    }
}
