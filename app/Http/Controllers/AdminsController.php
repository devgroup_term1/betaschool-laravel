<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;

class AdminsController extends Controller
{
    //

    public function index(){
        return Admin::all();
    }

    public function store(Request $request)
    {
        $admin = new Admin;
        $admin->firstName = $request->firstName;
        $admin->lastName = $request->lastName;
        $admin->user_id = $request->user_id;
        $admin->save();
        return response([
            'status' => 'success',
            'data' => $admin
        ], 200);
    }

    public function show($id)
    {
        if( Admin::find($id)!= null){
            return Admin::find($id);
        }else{
            return ['id' => 0];
        }
    }
}
