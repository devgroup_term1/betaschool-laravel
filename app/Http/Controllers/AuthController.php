<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use JWTAuth;
use App\User;
use App\Lecturer;
use App\Admin;
use App\Student;
use App\Http\Requests\RegisterFormRequest;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewUser;


class AuthController extends Controller
{
    //

    public function register(RegisterFormRequest $request)
    {
        $user = new User;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->role = $request->role;
        $user->save();
        return response([
            'status' => 'success',
            'data' => $user
        ], 200);
    }

    public function sendMail(Request $request){
    $email = $request->email;
    $password =$request->password;
    $role =$request->role;
    Mail::to($email)->send(new NewUser($password, $role));

    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)){
            $user = User::where('email', $credentials['email'])->firstOrFail();
            $token = JWTAuth::fromUser($user, ['role'=> $user->userDetails_type]);
             return response([
                'status' => 'success',
                'token'=> "Bearer $token",
                'role'=> $user->role
            ]);
        }

        else{
                return response([
                    'status' => 'error',
                    'error' => 'invalid.credentials',
                    'msg' => 'Invalid Credentials.'
                ], 400);
        }


    }
    public function user(Request $request)
    {
        $user = User::find(Auth::user()->id);
        return response([
                'status' => 'success',
                'data' => $user
            ]);
    }
    public function refresh()
    {
        return response([
                'status' => 'success'
            ]);
    }
}
