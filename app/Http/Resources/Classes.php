<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Http\Resources\Venue as VenueResource;
use App\Http\Resources\Lecturer as LecturerResource;
use App\Http\Resources\School as SchoolResource;
use App\Http\Resources\StudentCollection;
use App\Venue;
use App\Lecturer;
use App\Student;
use App\School;
use App\StudentMark;
use App\StudentAttendance;

class Classes extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return[
            'id' => $this->id,
            'name' => $this->name,
            
            'venue' => new VenueResource(Venue::find($this->venue_id)),
            'lecturer' => new LecturerResource(Lecturer::find($this->lecturer_id)),
            
            'startTime' => $this->startTime,
            'endTime' => $this->startTime + $this->duration,
            'day' => $this->day,
            'price' => $this->price,
            'students' => new StudentCollection($this->students),
            'school' => new SchoolResource(School::find($this->school_id)),
            'averageAttendance' => StudentAttendance::avg('attended'),
            'averageMark' => StudentMark::avg('mark'),
        ];
    }
}
