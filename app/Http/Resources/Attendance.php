<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Attendance extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->student->id,
            'student' => $this->student->firstName." ".$this->student->lastName,
            'term' => $this->term,
            'week' => $this->week,
            'attendance' => $this->attended
        ];
    }
}
