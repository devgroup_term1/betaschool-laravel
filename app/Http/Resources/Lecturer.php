<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\School as SchoolResource;
use App\School;

class Lecturer extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'user_id' => $this->user_id,
            'title' => $this->title,
            'firstName' => $this->firstName,
            'lastName' => $this->firstName,
            'gender' => $this->firstName,
            'qualifications' => $this->qualifications,
            'school' => new SchoolResource(School::find($this->school_id)),
            'dob' => $this->dob
        ];
    }
}
