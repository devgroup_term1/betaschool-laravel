<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Fees extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'studentNr' =>(int) $this->studentNr,
            'amountDue' =>(int) $this->amountDue,
            'amountPaid' =>(int) $this->amountPaid,
        ];
    }
}
