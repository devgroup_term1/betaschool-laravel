<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentMark extends Model
{
    public $timestamps = false;
    public function student(){
        return $this->belongsTo('App\Student');
    }
}
