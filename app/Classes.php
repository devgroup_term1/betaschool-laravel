<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{

    protected $fillable = ['id', 'venue_id', 'lecturer_id', 'name', 'startTime', 'duration', 'day', 'price', 'school_id'];

    public $timestamps = false;
    //classes has Many-to-many relation with students
    public function students(){
        return $this->belongsToMany('App\Student', 'student_classes', 'class_id', 'student_id');
    }

    public function lecturer(){
        return $this->belongsTo('App\Lecturer', 'user_id');
    }

    public function venue(){
        return $this->belongsTo('App\Venue');
    }

    public function attendance(){
        return $this->hasMany('App\StudentAttendance', 'class_id', 'id');
    }

    public function school(){
        return $this->hasOne('App\School', 'school_id', 'id');
    }
}
