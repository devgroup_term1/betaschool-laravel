<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    public function lectures(){
        return $this->hasMany('App\Lecturer');
    }

    public function classes(){
        return $this->hasMany('App\Classes');
    }

    public function students(){
        return $this->hasMany('App\Student');
    }
}
