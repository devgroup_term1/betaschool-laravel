# Betaschool

Betaschool is a school management system developed by Caitie Boeyens (160032) Brian Irons (170201) and Zandri Gillespie (170052).

## Installation

Make sure you have PHP installed and configured on your machine.

Use the package manager composer to install the dependencies.

```bash
composer install
```

## Database Setup

create a MySQL database for the project and set up your .env folder with your settings

Then run the following commands:

```bash
php artisan migrate:fresh

php artisan db:seed
```

## Usage

Launch the server

```bash
php artisan serve
```

After setting up the front end you can log in with any of the following users in order to explore the system:

Admin:  anderson@university.com; Adam Anderson

Student: m.smith@university.com; Monica Smith

Lecturer: bahr.a@university.com; Arthur Bahr

The password for all of the users is 123456
